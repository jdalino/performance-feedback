﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Internet.Models
{
    public class solutionContext : DbContext
    {
        public solutionContext()
            : base("DefaultConnection")
        { }
        public DbSet<CompanyModel> CompanyModel { get; set; }
    }

    public class solutionInitializer : CreateDatabaseIfNotExists<solutionContext> 
    { }
}